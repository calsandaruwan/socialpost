<?php
namespace App;
session_start();

include_once ('vendor/autoload.php');

define('baseUrl',"http://social2.dev");
define('fbAppId','');
define('fbAppSecret','');
define('twConsumerKey','');
define('twConsumerSecret','');

$route = new \App\Routes\BaseRoute();
