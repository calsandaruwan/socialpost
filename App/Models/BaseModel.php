<?php

namespace App\Models;

use App\Models\Connection;

class BaseModel
{
    public $_conn='';

    public function __construct()
    {
        $this->_conn = \App\Models\Connection::get();
    }

    public function query($query){
        $result = $this->_conn->query($query);
//        echo $query;
        if ($result){
            return $result;
        }
        return false;
    }

    public function get($query,$type=false){
        $result = $this->query($query);
        if($result && $result->num_rows>0){
            if($type=='row'){
                return $result->fetch_assoc();
            }else{
                $res_array=[];
                while($row = $result->fetch_assoc()){
                    $res_array[] = $row;
                }
                return $res_array;
            }
        }
        return false;
    }
}