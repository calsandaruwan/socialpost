<?php

namespace App\Routes;


class BaseRoute
{
    public function __construct()
    {
            $this->Route();
    }

    public function Route(){
        $segments =  $this->getSegments();

        //instantiate controller mentioned in the segment c
        //call method mentioned in the segment a
        include_once('App/Controllers/'.$segments['c'].'Controller.php');
        $controller =  'App\Controllers\\'.$segments['c'].'Controller';
        $obj = new $controller();
        $obj->{$segments['a']}();


    }

    public function getSegments(){
        //set default segment data
        $segments = [
          'c'=>'Auth',
          'a'=>'login'
        ];

        if(isset($_GET['c'])&&isset($_GET['a'])){
            $segments['c'] = ucfirst($_GET['c']);
            $segments['a'] = $_GET['a'];
        }

        return $segments;
    }
}