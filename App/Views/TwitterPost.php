<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <?= $data['title'] ?>
                </h4>
            </div>
            <div class="panel-body">
                <form method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="grnNo">Message:</label>
                                <textarea required name="post[message]" class="form-control"><?= isset($data['result']['message'])?($data['result']['message']):'' ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button name="submit" type="submit" class="btn btn-success pull-right">Post</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>