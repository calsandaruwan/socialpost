<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><?= $data['title'] ?></h4>
                </div>
                <div class="panel-body">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-6 tile">
                                <a style="font-size: 45px;padding: 100px 20px ;word-break: break-all; text-transform: capitalize;" class="btn btn-info btn-lg btn-block" href="<?= $data['fbLink'] ?>" >Continue with facebook</a>
                            </div>
                            <div  class="col-md-6 tile">
                                <a style="font-size: 45px;padding: 100px 20px ;word-break: break-all; text-transform: capitalize;" class="btn btn-info btn-lg btn-block" href="<?= $data['twitterLink'] ?>" >continue with twitter</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>