<?php

namespace App\Controllers;

use App\Controllers\BaseController as BaseController;
use Abraham\TwitterOAuth\TwitterOAuth;

class AuthController extends BaseController
{
    private $fb='';

    public function __construct() {
        $this->fb = new \Facebook\Facebook([
            'app_id' => fbAppId,
            'app_secret' => fbAppSecret,
            'default_graph_version' => 'v2.2',
        ]);
    }
    public function login(){
        $data['fbLink'] = $this->fbLink();
        $data['twitterLink'] = $this->twitterLink();;
        $data['title'] = 'SELECT NETWORK';
        if(isset($_GET['m'])){
            $data['msg'] =[
                'content'=>$_GET['m'],
                'type'=>'success'
            ];
        }
        $this->loadView('App/Views/Login.php',$data);
    }

    public function fbLink(){
        $helper = $this->fb->getRedirectLoginHelper();

        $permissions = ['publish_actions']; // Optional permissions
        $loginUrl = $helper->getLoginUrl(baseUrl.'?c=post&a=fb', $permissions);

        return htmlspecialchars($loginUrl);
    }

    public function twitterLink(){
        print_r($_SESSION);
        if(!isset($_SESSION['access_token'])) {
            $connection = new TwitterOAuth(twConsumerKey, twConsumerSecret);
            $access_token = $connection->oauth("oauth/request_token", ["oauth_callback" => baseUrl."?c=post&a=twitter"]);
            $_SESSION['access_token'] = $access_token;
            $url = $connection->url('oauth/authorize', ['oauth_token' => $access_token['oauth_token']]);
            return $url;
        }else{
            return baseUrl.'?c=post&a=twitter';
        }
    }
}