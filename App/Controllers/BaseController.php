<?php
/**
 * Created by PhpStorm.
 * User: cal
 * Date: 6/4/17
 * Time: 11:42 AM
 */

namespace App\Controllers;

class BaseController
{
    public function __construct()
    {
    }

    public function loadView($path='',$data){
        include_once('App/Views/Common/Layout.php');
    }
}