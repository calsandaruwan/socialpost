<?php

namespace App\Controllers;

use Abraham\TwitterOAuth\TwitterOAuth;
/**
 * Description of PostController
 *
 * @author cal
 */
class PostController extends BaseController {

    private $fb='';
    private $twitter;

    public function __construct()
    {
        $this->fb = new \Facebook\Facebook([
            'app_id' => fbAppId,
            'app_secret' => fbAppSecret,
            'default_graph_version' => 'v2.3',
        ]);
    }

    //twiter related functions


    //fb related functions

    public function fb(){
        if(isset($_POST['submit'])) {
            $this->getFbAccessToken();
            $post = $_POST['post'];
            //post to fb
            $linkData = [
                'name'=>$post['title'],
                'link' => $post['link'],
                'message' => $post['message'],
            ];

            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $this->fb->post('/me/feed', $linkData, $_SESSION['fb_access_token']);
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $graphNode = $response->getGraphNode();

            header('Location:'.baseUrl.'?c=auth&a=login&m=Successfully posted. ID - '.$graphNode['id']);

        }
        $data['title']="SHARE ON FACEBOOK";
        $DATA['RESULT']='';
        $this->loadView('App/Views/FbPost.php',$data);
    }


    public function getFbAccessToken(){

        $helper = $this->fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

//        var_dump($accessToken->getValue());

        $oAuth2Client = $this->fb->getOAuth2Client();
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        $tokenMetadata->validateAppId(fbAppId);
        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }

//            echo '<h3>Long-lived</h3>';
//            var_dump($accessToken->getValue());
        }

        $_SESSION['fb_access_token'] = (string) $accessToken;

    }

    public function twitter(){

        if(isset($_POST['submit'])) {
            $this->getTwitterConnection();
            $post = $_POST['post'];

            if($this->twitter->post('statuses/update', array('status' => $post['message']))){
                header('Location:'.baseUrl.'?c=auth&a=login&m=Successfully posted');
            }

        }
        $data['title']="SHARE ON TWITTER";
        $DATA['RESULT']='';
        $this->loadView('App/Views/TwitterPost.php',$data);
    }

    public function getTwitterConnection(){
        $connection = new TwitterOAuth(twConsumerKey, twConsumerSecret, $_GET['oauth_token'], $_GET['oauth_verifier']);
        $content = $connection->get('account/verify_credentials');
        $this->twitter = $connection;
    }
}


